#!/bin/bash

# Actualizează repo-urile
sudo dnf update -y

# Instalează GIMP
sudo dnf install -y gimp

# Instalează plugin-ul GIMP "G'MIC"
sudo dnf install -y gimp-gmic

# Instalează plugin-ul GIMP "Resynthesizer"
sudo dnf install -y gimp-resynthesizer

# Instalează plugin-ul GIMP "Liquid Rescale"
sudo dnf install -y gimp-lqr-plugin

# Instalează plugin-ul GIMP "GIMP DDS Plugin"
sudo dnf install -y gimp-dds

# Instalează plugin-ul GIMP "GIMP FX Foundry"
sudo dnf install -y gimpfx-foundry

# Instalează plugin-ul GIMP "GIMP Beautify"
sudo dnf install -y gimp-beautify

# Instalează plugin-ul GIMP "G'MIC-QT"
sudo dnf install -y gmic-qt

# Instalează pachetul GIMP "gimp-data-extras"
sudo dnf install -y gimp-data-extras

# Instalează pachetul GIMP "gimp-elsamuko"
sudo dnf install -y gimp-elsamuko

# Instalează pachetul GIMP "gimp-layer-via-copy-cut"
sudo dnf install -y gimp-layer-via-copy-cut

# Instalează pachetul GIMP "gimp-paint-studio"
sudo dnf install -y gimp-paint-studio

# Notificare finală
echo "Instalare completă!"

