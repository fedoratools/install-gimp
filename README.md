# Script de instalare a GIMP și a plugin-urilor pentru GIMP

Acest script bash automatizează procesul de instalare a GIMP și a unei serii de plugin-uri utile pentru GIMP folosind gestionarul de pachete DNF.

## Utilizare

1. Descărcați scriptul.
2. Deschideți un terminal și navigați la locația scriptului.
3. Dați permisiuni de execuție scriptului folosind comanda `chmod +x nume_script.sh`.
4. Executați scriptul folosind comanda `./nume_script.sh`.

## Descriere

Scriptul urmărește următoarele pași:

1. Actualizează repository-urile folosind `sudo dnf update -y`.
2. Instalează GIMP folosind `sudo dnf install -y gimp`.
3. Instalează următoarele plugin-uri pentru GIMP:

   - **G'MIC**: Adaugă o serie de filtre și instrumente de prelucrare a imaginilor avansate.
   - **Resynthesizer**: Oferă funcționalitate de clonare inteligentă, permițând eliminarea obiectelor nedorite dintr-o imagine.
   - **Liquid Rescale**: Permite redimensionarea imaginilor cu păstrarea proporțiilor esențiale.
   - **GIMP DDS Plugin**: Suport pentru editarea de texturi DDS (DirectDraw Surface) folosite frecvent în jocuri.
   - **GIMP FX Foundry**: Oferă o colecție largă de efecte suplimentare și instrumente de prelucrare.
   - **GIMP Beautify**: Adaugă opțiuni suplimentare pentru ajustarea și îmbunătățirea aspectului imaginilor.
   - **G'MIC-QT**: Interfață grafică pentru G'MIC, facilitând utilizarea filtrelor și efectelor G'MIC.
   - **Pachetul GIMP "gimp-data-extras"**: Adaugă imagini suplimentare și alte resurse utile pentru GIMP.
   - **Pachetul GIMP "gimp-elsamuko"**: Adaugă noi funcționalități și instrumente pentru GIMP.
   - **Pachetul GIMP "gimp-layer-via-copy-cut"**: Oferă opțiuni suplimentare pentru gestionarea straturilor de imagine.
   - **Pachetul GIMP "gimp-paint-studio"**: Adaugă pensule și setări suplimentare pentru o experiență îmbunătățită de pictură.

4. Afisează o notificare că instalarea este completă folosind `echo "Instalare completă!"`.
